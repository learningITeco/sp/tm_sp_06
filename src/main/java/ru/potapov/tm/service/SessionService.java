package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.ISessionService;
import ru.potapov.tm.dto.SessionDto;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.repository.ISessionRepository;
import ru.potapov.tm.util.SignatureUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Service
@Getter
@Setter
@Transactional
@NoArgsConstructor
public class SessionService extends AbstractService<Session, SessionDto> implements ISessionService {
    @Autowired
    @NotNull private ISessionRepository repository;

    @Override
    public @Nullable Session generateSession(@NotNull Session sessionUser, @NotNull String userId) {
        @Nullable Session sessionUserClone;
        try { sessionUserClone = (Session)sessionUser.clone(); }catch (Exception e){return null;}
        sessionUserClone.setSignature("");
        sessionUser.setSignature(SignatureUtil.sign(sessionUserClone,"", 1));
        sessionUser.setUser(getServiceLocator().getUserService().findOne(userId));
        //addSession(sessionUser, null);
        return sessionUser;
    }

    @NotNull
    @SneakyThrows
    public Collection<Session> collectionDtoToEntity(Collection<SessionDto> collectionSessionDto){
        Collection<Session> list = new ArrayList<>();
        for (SessionDto session : collectionSessionDto) {
            list.add(dtoToEntity(session));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Collection<SessionDto> collectionEntityToDto(Collection<Session> collectionSessionEntity){
        Collection<SessionDto> list = new ArrayList<>();
        for (Session session : collectionSessionEntity) {
            list.add(entityToDto(session));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Session dtoToEntity(SessionDto sessionDto){
        Session sessionEntity = new Session();
        sessionEntity.setId(sessionDto.getId());

        Optional<Session> optionalSession = getRepository().findById(sessionDto.getId());
        sessionEntity = ((optionalSession.isPresent())) ? optionalSession.get() : null;

        if (Objects.isNull(sessionEntity)){
            sessionEntity = new Session();
            sessionEntity.setId(sessionDto.getId());
            sessionEntity.setUser(getServiceLocator().getUserService().findOne(sessionDto.getUserId()) );
            sessionEntity.setSignature(sessionDto.getSignature());
            sessionEntity.setDateStamp(sessionDto.getDateStamp());
        }
        if (!sessionEntity.getUser().getId().equals(sessionDto.getUserId()))
            sessionEntity.setUser( getServiceLocator().getUserService().findOne(sessionDto.getUserId()) );
        if (!sessionEntity.getSignature().equals(sessionDto.getSignature()))
            sessionEntity.setSignature(sessionDto.getSignature());
        if (sessionEntity.getDateStamp() != sessionDto.getDateStamp())
            sessionEntity.setDateStamp(sessionDto.getDateStamp());

        return sessionEntity;
    }

    @Nullable
    @SneakyThrows
    public SessionDto entityToDto(Session sessionEntity){
        if (sessionEntity == null)
            return null;

        SessionDto sessionDto = new SessionDto();
        sessionDto.setId(sessionEntity.getId());

        if (Objects.nonNull(sessionEntity)){
            if (!sessionEntity.getUser().getId().equals(sessionDto.getUserId()))
                sessionDto.setUserId(sessionEntity.getUser().getId());
            if (!sessionEntity.getSignature().equals(sessionDto.getSignature()))
                sessionDto.setSignature(sessionEntity.getSignature());
            if (sessionEntity.getDateStamp() != sessionDto.getDateStamp())
                sessionDto.setDateStamp(sessionEntity.getDateStamp());

        }

        return sessionDto;
    }
}
