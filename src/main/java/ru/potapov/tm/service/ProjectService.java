package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.dto.ProjectDto;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.repository.IProjectRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Service
@Getter
@Setter
@Transactional
@NoArgsConstructor
public class ProjectService extends AbstractService<Project, ProjectDto> implements IProjectService {
    @Autowired
    @NotNull
    private IProjectRepository repository;


    @NotNull
    @SneakyThrows
    public Collection<Project> collectionDtoToEntity(Collection<ProjectDto> collectionProjectDto) {
        Collection<Project> list = new ArrayList<>();
        for (ProjectDto project : collectionProjectDto) {
            list.add(dtoToEntity(project));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Collection<ProjectDto> collectionEntityToDto(Collection<Project> collectionProjectEntity) {
        Collection<ProjectDto> list = new ArrayList<>();
        for (Project project : collectionProjectEntity) {
            list.add(entityToDto(project));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Project dtoToEntity(ProjectDto projectDto) {
        Project projectEntity = new Project();
        projectEntity.setId(projectDto.getId());

        Optional<Project> optionalProject = getRepository().findById(projectDto.getId());
        projectEntity = ((optionalProject.isPresent())) ? optionalProject.get() : null;

        if (Objects.isNull(projectEntity)) {
            projectEntity = new Project();
            projectEntity.setId(projectDto.getId());
            projectEntity = new Project();
            projectEntity.setDescription(projectDto.getDescription());
            projectEntity.setName(projectDto.getName());
            projectEntity.setStatus(projectDto.getStatus());
            projectEntity.setUser(getServiceLocator().getUserService().findOne(projectDto.getUserId()));
            projectEntity.setDateStart(projectDto.getDateStart());
            projectEntity.setDateFinish(projectDto.getDateFinish());
        }
        if (!projectEntity.getDescription().equals(projectDto.getDescription()))
            projectEntity.setDescription(projectDto.getDescription());
        if (!projectEntity.getName().equals(projectDto.getName()))
            projectEntity.setName(projectDto.getName());
        if (!projectEntity.getStatus().equals(projectDto.getStatus()))
            projectEntity.setStatus(projectDto.getStatus());
        if ((projectEntity.getUser() != null) && !projectEntity.getUser().getId().equals(projectDto.getUserId()))
            projectEntity.setUser(getServiceLocator().getUserService().findOne(projectDto.getUserId()));
        if (!projectEntity.getDateStart().equals(projectDto.getDateStart()))
            projectEntity.setDateStart(projectDto.getDateStart());
        if (!projectEntity.getDateFinish().equals(projectDto.getDateFinish()))
            projectEntity.setDateFinish(projectDto.getDateFinish());

        return projectEntity;
    }

    @Nullable
    @SneakyThrows
    public ProjectDto entityToDto(Project projectEntity) {
        if (projectEntity == null)
            return null;

        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(projectEntity.getId());

        if (Objects.nonNull(projectEntity)) {
            if (!projectEntity.getDescription().equals(projectDto.getDescription()))
                projectDto.setDescription(projectEntity.getDescription());
            if (!projectEntity.getName().equals(projectDto.getName()))
                projectDto.setName(projectEntity.getName());
            if (!projectEntity.getStatus().equals(projectDto.getStatus()))
                projectDto.setStatus(projectEntity.getStatus());
            if ((projectDto.getUserId() != null) && !projectEntity.getUser().getId().equals(projectDto.getUserId()))
                projectDto.setUserId(projectEntity.getUser().getId());
            if (!projectEntity.getDateStart().equals(projectDto.getDateStart()))
                projectDto.setDateStart(projectEntity.getDateStart());
            if (!projectEntity.getDateFinish().equals(projectDto.getDateFinish()))
                projectDto.setDateFinish(projectEntity.getDateFinish());
        }

        return projectDto;
    }
}
