package ru.potapov.tm.service;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.dto.UserDto;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.repository.IUserRepository;
import ru.potapov.tm.util.SignatureUtil;

import java.util.*;

@Service
@Getter
@Setter
@Transactional
public class UserService extends AbstractService<User, UserDto> implements IUserService {
//    @Autowired
    @NotNull private IUserRepository repository;

    @Override
    public @Nullable IUserRepository getRepository() {
        return (IUserRepository) super.getRepository();
    }

    public void createPredefinedUsers() {
        @NotNull String hashPass;

        hashPass = SignatureUtil.sign("1", "", 1);
        createUser("user", hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("2", "", 1);
        createUser("admin", hashPass, RoleType.Administrator);

        hashPass = SignatureUtil.sign("test", "", 1);
        createUser("test", hashPass, RoleType.Administrator);
    }

    @NotNull
    @Override
    public User createUser(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role) {
        @NotNull User user = new User();
        user.setRoleType(role);
        user.setLogin(name);
        user.setHashPass(hashPass);
        user.setId(UUID.randomUUID().toString());

        Session session = new Session();
        user.setSessionId(getServiceLocator().getSessionService().generateSession(session,user.getId()).getId());

        getRepository().save( user );
        return user;
    }

    @Override
    public @NotNull Collection<User> findAll() {
        return (Collection<User>) getRepository().findAllUsers();
    }

    @NotNull
    @SneakyThrows
    public Collection<User> collectionDtoToEntity(Collection<UserDto> collectionUserDto){
        Collection<User> list = new ArrayList<>();
        for (UserDto user : collectionUserDto) {
            list.add(dtoToEntity(user));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Collection<UserDto> collectionEntityToDto(Collection<User> collectionUserEntity){
        Collection<UserDto> list = new ArrayList<>();
        for (User user : collectionUserEntity) {
            list.add(entityToDto(user));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public User dtoToEntity(UserDto userDto){
        User userEntity = new User();
        userEntity.setId(userDto.getId());

        Optional<User> optionalUser = getRepository().findById(userDto.getId());
        userEntity = ((optionalUser.isPresent())) ? optionalUser.get() : null;

        if (Objects.isNull(userEntity)){
            userEntity = new User();
            userEntity.setId(userDto.getId());
            userEntity = new User();
            userEntity.setLogin( userDto.getLogin() );
            userEntity.setHashPass(userDto.getHashPass());
            if (Objects.nonNull(userDto.getSessionId()))
                userEntity.setSessionId(userDto.getSessionId());
            userEntity.setRoleType(userDto.getRoleType());
        }

        if (Objects.nonNull(userEntity.getLogin()) && !userEntity.getLogin().equals(userDto.getLogin()))
            userEntity.setLogin( userDto.getLogin() );
        if (Objects.nonNull(userEntity.getHashPass()) && !userEntity.getHashPass().equals(userDto.getHashPass()))
            userEntity.setHashPass(userDto.getHashPass());
        if (userDto.getSessionId() !=null)
            if (!userEntity.getSessionId().equals(userDto.getSessionId()));
        userEntity.setSessionId(userDto.getId());
        if (Objects.nonNull(userEntity.getRoleType()) && !userEntity.getRoleType().equals(userDto.getRoleType()));
        userEntity.setRoleType(userDto.getRoleType());

        return userEntity;
    }

    @Nullable
    @SneakyThrows
    public UserDto entityToDto(User userEntity){
        if (userEntity == null)
            return null;

        UserDto userDto = new UserDto();
        userDto.setId(userEntity.getId());

        if (Objects.nonNull(userEntity)){
            if (!userEntity.getLogin().equals(userDto.getLogin()))
                userDto.setLogin(userEntity.getLogin());
            if (!userEntity.getHashPass().equals(userDto.getHashPass()))
                userDto.setHashPass(userEntity.getHashPass());
            if (Objects.nonNull(userDto.getSessionId()) && userEntity.getSessionId() != userDto.getSessionId())
                userDto.setSessionId(getServiceLocator().getSessionService().findOne(userEntity.getSessionId()).getId());
            if (!userEntity.getRoleType().equals(userDto.getRoleType()))
                userDto.setRoleType(userEntity.getRoleType());
        }

        return userDto;
    }
}
