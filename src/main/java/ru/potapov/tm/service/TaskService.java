package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.dto.TaskDto;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.repository.ITaskRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Service
@Getter
@Setter
@Transactional
@NoArgsConstructor
public class TaskService extends AbstractService<Task, TaskDto> implements ITaskService {
    @Autowired
    @NotNull
    private ITaskRepository repository;

    @NotNull
    @SneakyThrows
    public Collection<Task> collectionDtoToEntity
            (@NotNull Collection<TaskDto> collectionTaskDto) {
        Collection<Task> list = new ArrayList<>();
        for (TaskDto task : collectionTaskDto) {
            list.add(dtoToEntity(task));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Collection<TaskDto> collectionEntityToDto
            (@NotNull Collection<Task> collectionTaskEntity) {
        Collection<TaskDto> list = new ArrayList<>();
        for (Task task : collectionTaskEntity) {
            list.add(entityToDto(task));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Task dtoToEntity(@NotNull TaskDto taskDto) {
        Task taskEntity = new Task();
        taskEntity.setId(taskDto.getId());

        Optional<Task> optionalTask = getRepository().findById(taskDto.getId());
        taskEntity = ((optionalTask.isPresent())) ? optionalTask.get() : null;

        if (Objects.isNull(taskEntity)) {
            taskEntity = new Task();
            taskEntity.setId(taskDto.getId());
            taskEntity = new Task();
            taskEntity.setDescription(taskDto.getDescription());
            taskEntity.setProject(getServiceLocator().getProjectService().findOne(taskDto.getProjectId()) );
            taskEntity.setName(taskDto.getName());
            taskEntity.setStatus(taskDto.getStatus());
            taskEntity.setUser(getServiceLocator().getUserService().findOne(taskDto.getUserId()));
            taskEntity.setDateStart(taskDto.getDateStart());
            taskEntity.setDateFinish(taskDto.getDateFinish());
        }
        if (!taskEntity.getDescription().equals(taskDto.getDescription()))
            taskEntity.setDescription(taskDto.getDescription());
        if ((taskEntity.getProject() != null) && !taskEntity.getProject().getId().equals(taskDto.getProjectId()))
            taskEntity.setProject(getServiceLocator().getProjectService().findOne(taskDto.getProjectId()));
        if (!taskEntity.getName().equals(taskDto.getName()))
            taskEntity.setName(taskDto.getName());
        if (!taskEntity.getStatus().equals(taskDto.getStatus()))
            taskEntity.setStatus(taskDto.getStatus());
        if ((taskEntity.getUser() != null) && !taskEntity.getUser().getId().equals(taskDto.getUserId()))
            taskEntity.setUser(getServiceLocator().getUserService().findOne(taskDto.getUserId()));
        if (!taskEntity.getDateStart().equals(taskDto.getDateStart()))
            taskEntity.setDateStart(taskDto.getDateStart());
        if (!taskEntity.getDateFinish().equals(taskDto.getDateFinish()))
            taskEntity.setDateFinish(taskDto.getDateFinish());

        return taskEntity;
    }

    @Nullable
    @SneakyThrows
    public TaskDto entityToDto(@Nullable Task taskEntity) {
        if (taskEntity == null)
            return null;

        TaskDto taskDto = new TaskDto();
        taskDto.setId(taskEntity.getId());

        if (Objects.nonNull(taskEntity)) {
            if (!taskEntity.getDescription().equals(taskDto.getDescription()))
                taskDto.setDescription(taskEntity.getDescription());
            if ((taskEntity.getProject() != null) && !taskEntity.getProject().getId().equals(taskDto.getProjectId()))
                taskDto.setProjectId(taskEntity.getProject().getId());
            if (!taskEntity.getName().equals(taskDto.getName()))
                taskDto.setName(taskEntity.getName());
            if (!taskEntity.getStatus().equals(taskDto.getStatus()))
                taskDto.setStatus(taskEntity.getStatus());
            if ((taskEntity.getUser() != null) && !taskEntity.getUser().getId().equals(taskDto.getUserId()))
                taskDto.setUserId(taskEntity.getUser().getId());
            if (!taskEntity.getDateStart().equals(taskDto.getDateStart()))
                taskDto.setDateStart(taskEntity.getDateStart());
            if (!taskEntity.getDateFinish().equals(taskDto.getDateFinish()))
                taskDto.setDateFinish(taskEntity.getDateFinish());
        }

        return taskDto;
    }
}
