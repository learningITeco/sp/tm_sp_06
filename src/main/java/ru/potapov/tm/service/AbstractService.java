package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.IService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.AbstractEntity;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Collection;
import java.util.Optional;

@Service
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity, U> implements IService<T, U> {

    @Autowired
    @Nullable CrudRepository<T, String> repository;

    @Autowired
    @Nullable ServiceLocator serviceLocator;

    @Override
    public int size() {
        if (repository == null)
            return 0;
        return (int) repository.count();
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String id) {
        if (repository == null)
            return null;

        @Nullable T t = null;

        Optional<T> optionalT = repository.findById(id);
        t = ((optionalT.isPresent())) ?  optionalT.get() : null;

        return t;
    }

    @Override
    public @NotNull Collection<T> findAll() {
        if (repository == null)
            return null;

        return (Collection<T>) repository.findAll();
    }

    @Override
    public String merge(@NotNull final T t) {
        if (repository == null)
            return "";

        repository.save(t);
        return t.getId();
    }

    @Override
    @Transactional
    public void persist(@NotNull final T t) {
        if (repository == null)
            return;

            repository.save(t);
    }

    @Override
    public void remove(@NotNull final T t) {
        if (repository == null)
            return;

        repository.delete(t);
    }

    @Override
    public void removeAll() {
        if (repository == null)
            return;

        repository.deleteAll();
    }
}
