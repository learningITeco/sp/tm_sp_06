package ru.potapov.tm.api;

import ru.potapov.tm.dto.TaskDto;
import ru.potapov.tm.entity.Task;

public interface ITaskService extends IService<Task, TaskDto> {
}
