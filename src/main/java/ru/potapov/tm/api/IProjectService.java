package ru.potapov.tm.api;

import ru.potapov.tm.dto.ProjectDto;
import ru.potapov.tm.entity.Project;

public interface IProjectService extends IService<Project, ProjectDto> {
}
