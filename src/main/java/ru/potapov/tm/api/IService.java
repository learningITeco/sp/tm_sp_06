package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.entity.AbstractEntity;

import java.util.Collection;

public interface IService<T extends AbstractEntity, U> {
    int size();
    @NotNull T findOne(@NotNull final String id);
    @NotNull Collection<T> findAll();
    String merge(@NotNull T t);
    void persist(@NotNull T t);
    void remove(@NotNull T t);
    void removeAll();

    @NotNull Collection<T> collectionDtoToEntity(Collection<U> collectionDto);
    @NotNull Collection<U> collectionEntityToDto(Collection<T> collectionEntity);
    @NotNull T dtoToEntity(U u);
    @NotNull U entityToDto(T t);
}
