package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Id;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractEntity {

    public abstract String getId();

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
