package ru.potapov.tm.util;

import com.hazelcast.core.HazelcastInstance;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
//@EnableCaching
@EnableTransactionManagement
@EnableJpaRepositories("ru.potapov.tm.repository")
@PropertySource("classpath:application.properties")
public class ConfigSpringDataJpa {

    @Bean
    public DataSource dataSource(
            @Value("${spring.datasource.driver-class-name}") final String dataSourceDriver,
            @Value("${spring.datasource.url}") final String dataSourceUrl,
            @Value("${spring.datasource.user}") final String dataSourceUser,
            @Value("${spring.datasource.password}") final String dataSourcePassword
    ) {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final DataSource dataSource,
            @Value("${spring.jpa.show-sql}") final boolean showSql,
            @Value("${spring.jpa.hibernate.ddl-auto}") final String tableStrategy,
            @Value("${spring.jpa.properties.hibernate.dialect}") final String dialect,

            @Value("${spring.jpa.properties.hibernate.cache.use_second_level_cache}") final boolean useSecondLavel,
            @Value("${spring.jpa.properties.hibernate.cache.use_query_cache}") final boolean useQueryCache,
            @Value("${spring.jpa.properties.hibernate.cache.region.factory_class}") final String factoryClass,

            @Value("${spring.jpa.properties.hibernate.cache.use_minimal_puts}") final boolean useMinimalPuts,
            @Value("${spring.jpa.properties.hibernate.cache.hazelcast.use_lite_member}") final boolean useLiteMember,
            @Value("${spring.jpa.properties.hibernate.cache.region_prefix}") final String regionPrefix,
            @Value("${spring.jpa.properties.hibernate.cache.provider_configuration_file_resource_path}") final String providerConfigurationFileResourcePath,

            @Value("${spring.jpa.properties.javax.persistence.sharedCache.mode}") final String mode
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.potapov.tm");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);
        properties.put("hibernate.dialect", dialect);

        properties.put("hibernate.cache.use_second_level_cache", useSecondLavel);
        properties.put("hibernate.cache.use_query_cache", useQueryCache);
        properties.put("hibernate.cache.region.factory_class", factoryClass);

        properties.put("hibernate.cache.region.use_minimal_puts", useMinimalPuts);
        properties.put("hibernate.cache.region.use_lite_member", useLiteMember);
        properties.put("hibernate.cache.region.region_prefix", regionPrefix);
        properties.put("hibernate.cache.region.provider_configuration_file_resource_path", providerConfigurationFileResourcePath);

        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@Autowired @NotNull LocalContainerEntityManagerFactoryBean emf ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf.getObject());
        return transactionManager;
    }

//    @Bean
//    public EntityManagerFactory createEntityManagerFactory(@Autowired LocalContainerEntityManagerFactoryBean containerEntityManagerFactoryBean){
//        return containerEntityManagerFactoryBean.getObject();
//    }

    @Bean
    public EntityManager createEntityManager(@Autowired LocalContainerEntityManagerFactoryBean emf){
        return emf.getObject().createEntityManager();
    }

//    Hazelcast
}
