package ru.potapov.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import java.text.SimpleDateFormat;
import java.util.Date;

@Setter
@Getter
@Controller
//@RequestMapping(value = "project")
@ComponentScan(basePackages = "ru.potapov.tm")
public class ProjectController {

    @NotNull private final Logger logger = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    @NotNull ServiceLocator serviceLocator;

    public ProjectController() {}

    // list page
    @RequestMapping(value = "/project", method = RequestMethod.POST)
    public String showAllProjects(Model model) {
        logger.debug("showAllProjects()");
        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());
        return "projects/list";
    }

    //projects
    @RequestMapping(value = "/projects", method = RequestMethod.POST)
    public String saveOrUpdateProject(@ModelAttribute("projectForm") @Validated @NotNull Project project,
                                   BindingResult result, Model model,
                                   final RedirectAttributes redirectAttributes) {

        logger.debug("saveOrUpdateProject() : {}", project);
        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "projects/projectform";
        } else {
            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            if(project.isNew()){
                redirectAttributes.addFlashAttribute("msg", "Project added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "Project updated successfully!");
            }
            serviceLocator.getProjectService().merge(project);

            populateDefaultModel(model);

            return "projects/list";
        }
    }

    // show add project form
    @RequestMapping(value = "/projects/add", method = RequestMethod.GET)
    public String showAddProjectForm(Model model) {
        logger.debug("showAddProjectForm()");

        @NotNull Project project = new Project();
        // set default value
        project.setName("");
        project.setDescription("D22");
        project.setStatus(Status.Planned);
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        model.addAttribute("projectForm", project);
        populateDefaultModel(model);

        populateDefaultModel(model);

        return "projects/projectform";
    }

    // show update form
    @RequestMapping(value = "/projects/{id}/update", method = RequestMethod.GET)
    public String showUpdateProjectForm(@PathVariable("id") String id, Model model) {
        logger.debug("showUpdateProjectForm() : {}", id);

        @NotNull final Project project = serviceLocator.getProjectService().findOne(id);
        model.addAttribute("projectForm", project);

        populateDefaultModel(model);

        return "projects/projectform";
    }

    // delete project
    @RequestMapping(value = "/projects/{id}/delete", method = RequestMethod.GET)
    public String deleteProject(@PathVariable("id") String id, Model model, RedirectAttributes redirectAttributes) {

        logger.debug("deleteProject() : {}", id);
        @NotNull Project project = serviceLocator.getProjectService().findOne(id);
        if (project != null)
            serviceLocator.getProjectService().remove(project);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Project is deleted!");

        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());

        return "projects/list";        
    }

     private void populateDefaultModel(Model model) {
        model.addAttribute("userList", serviceLocator.getUserService().findAll());
        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());
        model.addAttribute("taskList", serviceLocator.getTaskService().findAll());
        model.addAttribute("sessionList", serviceLocator.getSessionService().findAll());

        model.addAttribute("statuses", Status.values());
        model.addAttribute("roles", RoleType.values());
    }
}
