package ru.potapov.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.potapov.tm.api.*;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import java.text.SimpleDateFormat;
import java.util.Date;

@Setter
@Getter
@Controller
@ComponentScan(basePackages = "ru.potapov.tm")
public class MainController implements ServiceLocator {
    @Nullable User curUser;
    @Nullable Project curProject;

    @Autowired
    @NotNull IProjectService projectService;

    @Autowired
    @NotNull ITaskService taskService;

    @Autowired
    @NotNull IUserService userService;

    @Autowired
    @NotNull ISessionService sessionService;

    @NotNull private final Logger logger = LoggerFactory.getLogger(MainController.class);

    public MainController() {
       // init();
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("start");

        return modelAndView;
    }

    private void populateDefaultModel(Model model) {
        model.addAttribute("userList", getUserService().findAll());
        model.addAttribute("projectList", getProjectService().findAll());
        model.addAttribute("taskList", getTaskService().findAll());
        model.addAttribute("sessionList", getSessionService().findAll());

        model.addAttribute("statuses", Status.values());
        model.addAttribute("roles", RoleType.values());
    }

    public void init(){
        if (getUserService().findAll().size() != 0)
            return;

        createUsers();
        createProjects();
        createTasks();
    }

    public void createUsers(){
//        if (getUserService().findAll().size() > 0)
//            return;

        getUserService().createPredefinedUsers();
    }
    public void createProjects(){
        if (getProjectService().findAll().size() > 0)
            return;

        Project project = new Project();
        project.setName("P1");
        project.setDescription("D1");
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        project.setStatus(Status.Planned);
        project.setUser(curUser);
        getProjectService().persist(project);
        curProject = project;

    }
    public void createTasks(){
        if (getTaskService().findAll().size() > 0)
            return;

        Task task = new Task();
        task.setName("Task1");
        task.setProject(curProject);
        task.setDescription("Description T1");
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        task.setStatus(Status.Planned);
        task.setUser(curUser);
        getTaskService().persist(task);
    }

}
