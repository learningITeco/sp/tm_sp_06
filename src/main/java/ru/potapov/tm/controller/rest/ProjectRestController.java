package ru.potapov.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.ProjectDto;
import ru.potapov.tm.entity.Project;

import java.util.Collection;

@RestController
@RequestMapping("rest/project")
public class ProjectRestController {
    @Autowired
    @NotNull ServiceLocator serviceLocator;

    @GetMapping(produces = "application/json")
    public ResponseEntity<Collection<ProjectDto>> getProjectList(){
        Collection<Project>     projectList = serviceLocator.getProjectService().findAll();
        Collection<ProjectDto>  projectDtoList = serviceLocator.getProjectService().collectionEntityToDto(projectList);
        return ResponseEntity.ok(projectDtoList);
    }

    @GetMapping(value = "{projectId}", produces = "application/json")
    public ResponseEntity<ProjectDto> getProject(@PathVariable String projectId){
        try {
            Project project         = serviceLocator.getProjectService().findOne(projectId);
            ProjectDto projectDto   = serviceLocator.getProjectService().entityToDto(project);
            return ResponseEntity.ok(projectDto);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<ProjectDto> createProject(@RequestBody ProjectDto projectDto){
        Project project = serviceLocator.getProjectService().dtoToEntity(projectDto);
        projectDto.setId(serviceLocator.getProjectService().merge(project));
        return ResponseEntity.ok(projectDto);
    }

    @PutMapping(
            path = "{projectId}",
            consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<Void> modifyProject(@PathVariable String projectId, @RequestBody ProjectDto projectDto){
        Project project = serviceLocator.getProjectService().dtoToEntity(projectDto);
        if (project == null)
            return ResponseEntity.notFound().build();

        serviceLocator.getProjectService().merge(project);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{projectId}")
    public ResponseEntity<Void> deleteProject(@PathVariable String projectId){
        Project project = serviceLocator.getProjectService().findOne(projectId);
        if (project == null)
            return ResponseEntity.notFound().build();

        serviceLocator.getProjectService().remove(project);
        return ResponseEntity.noContent().build();
    }
}
