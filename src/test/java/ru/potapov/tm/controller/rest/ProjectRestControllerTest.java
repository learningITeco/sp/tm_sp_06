package ru.potapov.tm.controller.rest;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.potapov.tm.dto.ProjectDto;
import ru.potapov.tm.dto.UserDto;
import ru.potapov.tm.enumeration.Status;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProjectRestControllerTest extends AbstractTest{
    ProjectDto curProject;

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }
    
    @Test
    public void getProjectsList() throws Exception {
        String uri = "/rest/project";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        ProjectDto[] projectlist = super.mapFromJson(content, ProjectDto[].class);
        assertTrue(projectlist.length > 0);
    }

    @Test
    public void operationOnProject() throws Exception {
        createProject();
        updateProject();
        deleteProject();
    }
    
    public void createProject() throws Exception {
        String uri = "/rest/project";
        
        ProjectDto project = new ProjectDto();
        project.setId(UUID.randomUUID().toString());
        project.setName("Test");
        project.setDescription("Test description");
        project.setStatus(Status.Planned);
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        project.setUserId("");

        String inputJson = super.mapToJson(project);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
    
        ProjectDto newProject = super.mapFromXml(content, ProjectDto.class);
        curProject = newProject;
        assertEquals(project.getName(), newProject.getName());
    }

    public void updateProject() throws Exception {
        String uri = "/rest/project/" + curProject.getId();
        curProject.setName("Lemon");

        String inputJson = super.mapToJson(curProject);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(204, status);

        ProjectDto newProject = getProjectById(curProject.getId());
        assertEquals(curProject.getName(), newProject.getName());
    }

    public void deleteProject() throws Exception {
        String uri = "/rest/project/" + curProject.getId();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(204, status);

        ProjectDto newProject = getProjectById(curProject.getId());
        assertEquals(newProject, null);
    }

    public ProjectDto getProjectById(String id)throws Exception{
        String uri = "/rest/project/" + id;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        if (content.isEmpty())
            return null;

        return super.mapFromJson(content, ProjectDto.class);
    }
}