package ru.potapov.tm.controller.rest;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.potapov.tm.dto.TaskDto;
import ru.potapov.tm.enumeration.Status;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;

public class TaskRestControllerTest extends AbstractTest{
    TaskDto curTask;

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void getTasksList() throws Exception {
        String uri = "/rest/task";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        TaskDto[] tasklist = super.mapFromJson(content, TaskDto[].class);
        assertTrue(tasklist.length > 0);
    }

    @Test
    public void operationOnTask() throws Exception {
        createTask();
        updateTask();
        deleteTask();
    }

    public void createTask() throws Exception {
        String uri = "/rest/task";

        TaskDto task = new TaskDto();
        task.setId(UUID.randomUUID().toString());
        task.setName("Test");
        task.setDescription("Test description");
        task.setProjectId("");
        task.setStatus(Status.Planned);
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        task.setUserId("");

        String inputJson = super.mapToJson(task);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        TaskDto newTask = super.mapFromXml(content, TaskDto.class);
        curTask = newTask;
        assertEquals(task.getName(), newTask.getName());
    }

    public void updateTask() throws Exception {
        String uri = "/rest/task/" + curTask.getId();
        curTask.setName("Lemon");

        String inputJson = super.mapToJson(curTask);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(204, status);

        TaskDto newTask = getTaskById(curTask.getId());
        assertEquals(curTask.getName(), newTask.getName());
    }

    public void deleteTask() throws Exception {
        String uri = "/rest/task/" + curTask.getId();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(204, status);

        TaskDto newTask = getTaskById(curTask.getId());
        assertEquals(newTask, null);
    }

    public TaskDto getTaskById(String id)throws Exception{
        String uri = "/rest/task/" + id;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        if (content.isEmpty())
            return null;

        return super.mapFromJson(content, TaskDto.class);
    }
}