package ru.potapov.tm.controller.rest;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.potapov.tm.dto.ProjectDto;
import ru.potapov.tm.dto.UserDto;
import ru.potapov.tm.enumeration.RoleType;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;

public class UserRestControllerTest extends AbstractTest{
    UserDto curUser;

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void getUsersList() throws Exception {
        String uri = "/rest/user";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        UserDto[] userlist = super.mapFromJson(content, UserDto[].class);
        assertTrue(userlist.length > 0);
    }

    @Test
    public void operationOnUser() throws Exception {
        createUser();
        updateUser();
        deleteUser();
    }

    public void createUser() throws Exception {
        String uri = "/rest/user";

        UserDto user = new UserDto();
        user.setId(UUID.randomUUID().toString());
        user.setLogin("Test");
        user.setHashPass("HashPass");
        user.setRoleType(RoleType.User);
        user.setSessionId("");

        String inputJson = super.mapToJson(user);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        UserDto newUser = super.mapFromXml(content, UserDto.class);
        curUser = newUser;
        assertEquals(user.getLogin(), newUser.getLogin());
    }

    public void updateUser() throws Exception {
        String uri = "/rest/user/" + curUser.getId();
        curUser.setLogin("Lemon");

        String inputJson = super.mapToJson(curUser);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(204, status);

        UserDto newUser = getUserById(curUser.getId());
        assertEquals(curUser.getLogin(), newUser.getLogin());
    }

    public void deleteUser() throws Exception {
        String uri = "/rest/user/" + curUser.getId();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(204, status);

        UserDto newUser = getUserById(curUser.getId());
        assertEquals(newUser, null);
    }

    public UserDto getUserById(String id)throws Exception{
        String uri = "/rest/user/" + id;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        if (content.isEmpty())
            return null;

        return super.mapFromJson(content, UserDto.class);
    }
}